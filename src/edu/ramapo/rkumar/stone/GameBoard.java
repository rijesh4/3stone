package edu.ramapo.rkumar.stone;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class GameBoard extends Activity {
	
	private Cell curCell = new Cell(-1, -1); 
	private Cell bestChosenCell = new Cell (-1,-1); 
	
	private boolean humTurn = true; 
	private Board curBoard; 
	private HumPlayer humPlayer; 
	private CompPlayer compPlayer; 
	//private Cell[] allCells; 
	private String chosenColor = "r";
	ParcelData pd = new ParcelData(); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_board);
		
		
		curBoard = new Board(); 
		humPlayer = new HumPlayer();
		compPlayer = new CompPlayer();
		discardSuperfluousButtons();
		
		Intent intent = getIntent(); 
		
		pd = (ParcelData)intent.getSerializableExtra("pdVal"); 
		humTurn = pd.retHumTurn(); 
		humPlayer.score = pd.retHumScore(); 
		compPlayer.score = pd.retCompScore();
		humPlayer.blackStone = pd.retStones(true, 0); 
		humPlayer.whiteStone = pd.retStones(true, 1);
		humPlayer.clearStone = pd.retStones(true, 2);
		
		compPlayer.blackStone = pd.retStones(false, 0);
		compPlayer.whiteStone = pd.retStones(false, 1);
		compPlayer.clearStone = pd.retStones(false, 2);
		
		humPlayer.color = pd.retHumColor(); 
		compPlayer.color = pd.retCompColor();
		
		if (!(pd.retGameType()).equals("ng") ) {
			curCell = pd.retCurCell();
			startGame(false);
		}
		else {
			startGame(true);
		}
		 
		 
	}
	
	/** 
	 * Function Name: StartGame
	   Purpose: Init a game 
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) If new game find a random position to play for the computer or hint the human likewise
	2) update the data accordingly  
	3) if not a new game update the board according to the data
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void startGame(boolean isNewGame) {
		//Player curPlayer = new Player();
		Random rand = new Random();
		int randVal = rand.nextInt(80);
		curCell =  curBoard.retCell(randVal);
		
		EditText textView = (EditText)findViewById(R.id.e_depth);
		textView.setText("1");
		
		if (isNewGame) {
			if (!humTurn) {
				 
				randVal = rand.nextInt(80);
				bestChosenCell = curBoard.retCell(randVal); 
				bestChosenCell.color = compPlayer.color; 
				String message = "Computer best tile to play: Tile of color " + bestChosenCell.color + " on : ( " + bestChosenCell.xPos + " , " + bestChosenCell.yPos + " )" ; 
				displayScreen();
				updateColorButton();
			}
			else {
				displayScreen(); 
				updateColorButton();
			}
		}
		else {
			//Setup Board
			//Setup CurCell. 
			
			for (int j = 0 ; j < 80 ; j ++) {
				curBoard.retCell(j).color = pd.curBoard.get(j);
			}
			
			for (int j = 0 ; j < 80 ; j++) {
			ViewGroup viewGroup = (ViewGroup)findViewById(R.id.i01).getParent();
			int pos = j + 1; 
			String sPos = ""; 
			if (pos < 10 && pos >= 0 ) {
				sPos += "0" + pos; 
			}
			else {
				sPos += pos; 
			}
			
			//viewGroup.getChildAt(curBoard.cellNumToPosConverter(bestChosenCell)-10).getBackground().setColorFilter(0xCCCC3399, PorterDuff.Mode.MULTIPLY); 
			for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
				if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'a' && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(1) == sPos.charAt(0) && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(2) == sPos.charAt(1) ) {
					if (curBoard.retCell(pos-1).color.equals("b")) {
						findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xFF000000, PorterDuff.Mode.MULTIPLY);
						break;
					}
					else if (curBoard.retCell(pos-1).color.equals("w")) {
						findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xFF003399, PorterDuff.Mode.MULTIPLY);
						break;
					}
					else if (curBoard.retCell(pos-1).color.equals("c")) {
						findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xCCCC3399, PorterDuff.Mode.MULTIPLY);
						break;
					}
					
				} 
			}
			
			}
			curCell = curBoard.retCell(curBoard.cellNumToPosConverter(pd.curCell)) ;	
			curBoard.calcScore();
			displayScreen(); 		
			updateColorButton();
			
		}
		//in a loop hPLayer or cPlayer Next Move; 
	}

/** 
	 * Function Name: initSave
	   Purpose: Populate the data to save to a file  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void initSave (View view) {
		String dump = ""; 
		dump += "Human Player: Black " + humPlayer.score + " " +humPlayer.blackStone + " " + humPlayer.whiteStone + " "+humPlayer.clearStone + "\r\n" ;
		dump += "Computer Player: White " + compPlayer.score + " " +compPlayer.blackStone + " " + compPlayer.whiteStone + " "+compPlayer.clearStone + "\r\n" ;
		if (humTurn) {
			dump += "Next Player: Human " + curCell.xPos + " " + curCell.yPos + "\r\n"; 	
		}
		else {
			dump += "Next Player: Computer " + curCell.xPos + " " + curCell.yPos + "\r\n";
 		}
	
		dump += "Board: " + "\r\n"; 
		
		int counter = 3; 
		int tempCounter = 0; 
		for (int i = 0 ; i < 11 ; i++) {
			for (int j = 0 ; j < counter ; j++) {
				if ( curBoard.retCell(tempCounter+j).color.equals("r") ) {
					dump += "O ";
				}
				else if (curBoard.retCell(tempCounter+j).color.equals("b")) {
					dump += "B "; 
				}
				else if (curBoard.retCell(tempCounter+j).color.equals("w")) {
					dump += "W ";
				}
				else if (curBoard.retCell(tempCounter+j).color.equals("c")) {
					dump += "C ";
				}
			}
			
			tempCounter += counter; 
			if (i <= 3 ) {
				counter += 2; 
			}
			else if (i == 5) {
				counter += 1; 
			}
			/*else if (i == 6) {
				counter += 1; 
			}*/
			else if (i >= 6 ) {
				counter -= 2; 
			}
			else if (i == 4) {
				counter -=1; 
			}
			
			System.out.println(dump); 
			dump+= "\r\n"; 
			
		}
		
	dump += " "; 
	
	try {
		File logFile = new File(Environment.getExternalStorageDirectory().toString(), "myFile123.txt");
		if(!logFile.exists()) {
		     logFile.createNewFile();
		}

		BufferedWriter output = new BufferedWriter(new FileWriter(logFile));
		output.write(dump);
		output.close();
		
	} catch (Exception e) {
	        e.printStackTrace();
	}
	android.os.Process.killProcess(android.os.Process.myPid());
    System.exit(1); 
		
		
	}
	/*
	public void initNextMove (Player p1) {	
	}*/
	

	public void displayMessage() {
		TextView textView = (TextView)findViewById(R.id.t_turn);
		String message = "Turn: "; 
		//String note = ""; 
		String tColor = ""; 
		if (humTurn) {
			message += "HUMAN " + humPlayer.color; 
		}
		else {
			message += "COMPUTER " + compPlayer.color;
		}		
		textView.setText(message); 
		textView = (TextView)findViewById(R.id.t_hint);
		message = "";
		message += "best tile to play: Tile of color " +  bestChosenCell.retTempColor() + " on : ( " + bestChosenCell.xPos + " , " + bestChosenCell.yPos + " )" ;;  
		textView.setText(message);
	}

/** 
	 * Function Name: displayScreen
	   Purpose: Update screen text after commit  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Find The arrangement vertHor and diagonals for each cell
	2) If a valid arrangement increase the score  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void displayScreen() {
		TextView textView = (TextView)findViewById(R.id.t_turn);
		String message = "Turn: "; 
		if (humTurn) {
			message += "HUMAN " + humPlayer.color; 
		}
		else {
			message += "COMPUTER " + compPlayer.color;
		}
		textView.setText(message); 
		textView = (TextView)findViewById(R.id.t_hint);
		message = " ";
		curBoard.calcScore(); 
		if (humPlayer.color.equals("w")) {
			message += "HumanScore: " + curBoard.WScore + "\n\r" ;
			message += "CompScore: " + curBoard.BScore ;
		}
		else {
			message += "CompScore: " + curBoard.WScore + "\n\r";
			message += "HumScore: " + curBoard.BScore ;
		}
		 
		textView.setText(message);
		
	}	

/** 
	 * Function Name: initCommit
	   Purpose: Commit to the move selected by the user or best chosen from the computer  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Get the bestChosenTile and update the view 
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void initCommit (View view) {		
		
		if (!humTurn) {
			bestChosenCell.color = bestChosenCell.retTempColor();
			if (bestChosenCell.color.equals("b")) {
				compPlayer.blackStone -- ; 
			}
			else if (bestChosenCell.color.equals("w")) {
				compPlayer.whiteStone -- ; 
			}
			else if (bestChosenCell.color.equals("c")) {
				compPlayer.clearStone --; 
			}
		}
		else {
			if (bestChosenCell.color.equals("b")) {
				humPlayer.blackStone -- ; 
			}
			else if (bestChosenCell.color.equals("w")) {
				humPlayer.whiteStone -- ; 
			}
			else if (bestChosenCell.color.equals("c")) {
				humPlayer.clearStone --; 
			}
			
		}
		//bestChosenCell.color = bestChosenCell.retTempColor(); 
		curBoard.setCell(bestChosenCell); 
		
		
		ViewGroup viewGroup = (ViewGroup)findViewById(R.id.i01).getParent();
		int pos = curBoard.cellNumToPosConverter(bestChosenCell) + 1; 
		String sPos = ""; 
		if (pos < 10 && pos >= 0 ) {
			sPos += "0" + pos; 
		}
		else {
			sPos += pos; 
		}
		
		//viewGroup.getChildAt(curBoard.cellNumToPosConverter(bestChosenCell)-10).getBackground().setColorFilter(0xCCCC3399, PorterDuff.Mode.MULTIPLY); 
		for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
			if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'a' && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(1) == sPos.charAt(0) && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(2) == sPos.charAt(1) ) {
				if (bestChosenCell.color.equals("b")) {
					findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xFF000000, PorterDuff.Mode.MULTIPLY);
				}
				else if (bestChosenCell.color.equals("w")) {
					findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xFF003399, PorterDuff.Mode.MULTIPLY);
				}
				else if (bestChosenCell.color.equals("c")) {
					findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0xCCCC3399, PorterDuff.Mode.MULTIPLY);
				}
				
			} 
		}
		
		curCell = bestChosenCell; 
		
		if (humTurn) {
			humTurn = false; 
		} 
		else {
			humTurn = true; 
		}
		
		removeHeur(); 
		curBoard.resetAllTempData(); 
		displayScreen(); 
		updateColorButton();
		checkEnd();
	}

/** 
	 * Function Name: CheckEnd
	   Purpose: Check if the game has ended  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) If sum of all tiles in the hands of comp and human is 0 then return true .. update screen
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void checkEnd() {
		if (compPlayer.whiteStone +compPlayer.blackStone + compPlayer.clearStone <= 1 || humPlayer.whiteStone +humPlayer.blackStone + humPlayer.clearStone <= 1 ) {
			 
			TextView textView = (TextView)findViewById(R.id.t_hint);
			String message = " ";
			if (humPlayer.score > compPlayer.score) {
				message += "The winner is : Human. "; 
			}
			else if (humPlayer.score < compPlayer.score) {
				message += "The winner is : Computer. ";
			}
			else {
				message += "The winner is : Draw. ";
			}
			message += humPlayer.score + " ";
			message += compPlayer.score + " ";
			textView.setText(message);
		}
	}
	
/** 
	 * Function Name: updateColorButton
	   Purpose: Update the number of tiles left in the hands of players and display on the view
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void updateColorButton() {		
		Button button = (Button)findViewById(R.id.b_black); 
		
		if (humTurn) {
			button.setText("Black: " + humPlayer.blackStone);
		}
		else {
			button.setText("Black: " + compPlayer.blackStone);
		}
		//
		button = (Button)findViewById(R.id.b_white); 
		
		if (humTurn) {
			button.setText("White: " + humPlayer.whiteStone);
		}
		else {
			button.setText("White: " + compPlayer.whiteStone);
		}
		//
		button = (Button)findViewById(R.id.b_clear); 
		
		if (humTurn) {
			button.setText("Clear: " + humPlayer.clearStone);
		}
		else {
			button.setText("Clear: " + compPlayer.clearStone);
		}
		 
	}
	
	/** 
	 * Function Name: chooseCellColor
	   Purpose: Update the chosen color for find the heuristics of the valid cells  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void chooseCellColor (View view) {
		
		switch(view.getId()) {
		case R.id.b_black:
			chosenColor = "b"; 
			break; 
		case R.id.b_white:
			chosenColor = "w"; 
			break; 
		case R.id.b_clear:
			chosenColor = "c"; 
			
		}
	}

/** 
	 * Function Name: removeHeuristics
	   Purpose: Clear the heuristics value shown on the buttons  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void removeHeur () {
		
		ViewGroup viewGroup = (ViewGroup)findViewById(R.id.i01).getParent();
		
		//Cell temp = new Cell (-1,-1); 
		int pos = 0; 
		Button b; 
		//String message  = ""; 
		
		for (int j = 0 ; j < 80 ; j++) {
			pos = curBoard.cellNumToPosConverter(curBoard.boardCells[j]);
			pos += 1; 
			String sPos = ""; 
			if (pos < 10 && pos >= 0 ) {
				sPos += "0" + pos; 
			}
			else {
				sPos += pos; 
			}
			for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
				if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'a' && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(1) == sPos.charAt(0) && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(2) == sPos.charAt(1) ) {
					//if (bestChosenCell.color.equals("b")) {
					b = (Button)findViewById(viewGroup.getChildAt(i).getId());
					//aTree.computeSingleHeuristics(curBoard.retAllCells(), aList.get(j), chosenColor); 
					b.setText(""); 
				} 
			}
		}
		
	}
	
	/** 
	 * Function Name: displayCurHeuristics
	   Purpose: Display the heuristics value on the valid moves buttons   
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void displayCurHeur(){
		curBoard.resetAllTempData();
		
		Tree aTree;// = new Tree(curCell);
		int wiList[] = {15,15,6} ;
		int biList[] = {15,15,6} ;
		
		if (humPlayer.color.equals("w")) {
			wiList[0] = humPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			wiList[1] = humPlayer.blackStone; 
			wiList[2] = humPlayer.clearStone;
			
			biList[0] = compPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			biList[1] = compPlayer.blackStone; 
			biList[2] = compPlayer.clearStone;
			
			//aTree = new Tree (curCell, wiList, biList); 
			
		}
		else if (compPlayer.color.equals("w")) {
			wiList[0] = compPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			wiList[1] = compPlayer.blackStone; 
			wiList[2] = compPlayer.clearStone;
			
			biList[0] = humPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			biList[1] = humPlayer.blackStone; 
			biList[2] = humPlayer.clearStone;
			
			//aTree = new Tree (curCell, wiList, biList);
		}
		
		aTree = new Tree (curCell, wiList, biList);
		
		ArrayList<Cell> aList = new ArrayList();
		aList =	aTree.retValidMoveCells(curCell, curBoard.retAllCells());
		ViewGroup viewGroup = (ViewGroup)findViewById(R.id.i01).getParent();
		
		//Cell temp = new Cell (-1,-1); 
		int pos = 0; 
		Button b; 
		String message  = ""; 
		
		for (int j = 0 ; j < aList.size() ; j++) {
			pos = curBoard.cellNumToPosConverter(aList.get(j));
			pos+=1; 
			String sPos = ""; 
			if (pos < 10 && pos >= 0 ) {
				sPos += "0" + pos; 
			}
			else {
				sPos += pos; 
			}
			for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
				if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'a' && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(1) == sPos.charAt(0) && getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(2) == sPos.charAt(1) ) {
					//if (bestChosenCell.color.equals("b")) {
					b = (Button)findViewById(viewGroup.getChildAt(i).getId());
					aTree.computeSingleHeuristics(curBoard.retAllCells(), aList.get(j), chosenColor); 
					
					if (humTurn) {
						if (humPlayer.color.equals("w")) {
							b.setText(message + aList.get(j).heurVal[0])	;
						}
						else {
							b.setText(message + aList.get(j).heurVal[1]);
						}
					}
					else {
						if (compPlayer.color.equals("w")) {
							b.setText(message + aList.get(j).heurVal[0])	;
						}
						else {
							b.setText(message + aList.get(j).heurVal[1]);
						}
					}
					message = ""; 
				} 
			}
		}
		 
		
		//for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
			//if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'i'  ) {
				//findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0x00FFFFFF, PorterDuff.Mode.MULTIPLY);
		
 
		
		
		
	}


	/** 
	 * Function Name: compute
	   Purpose: comute the heuristic value, compute the best possible move, and update  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Create a Tree with the cur Cell
	2) Call minMax, and get the bestChosenCell to display
	3) Update the cell for all valid moves.   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void computeScore(View view) {
		 
		Tree curTree;// = new Tree(curCell);
		int wiList[] = {15,15,6} ;
		int biList[] = {15,15,6} ;
		int aDepth = 1; 
		EditText textView = (EditText)findViewById(R.id.e_depth);
		aDepth = Integer.parseInt(textView.getText().toString());
		
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		
		
		if (humPlayer.color.equals("w")) {
			wiList[0] = humPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			wiList[1] = humPlayer.blackStone; 
			wiList[2] = humPlayer.clearStone;
			
			biList[0] = compPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			biList[1] = compPlayer.blackStone; 
			biList[2] = compPlayer.clearStone;
			
			//aTree = new Tree (curCell, wiList, biList); 
			
		}
		else if (compPlayer.color.equals("w")) {
			wiList[0] = compPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			wiList[1] = compPlayer.blackStone; 
			wiList[2] = compPlayer.clearStone;
			
			biList[0] = humPlayer.whiteStone ; //, humPlayer.blackStone , humPlayer.clearStone};  
			biList[1] = humPlayer.blackStone; 
			biList[2] = humPlayer.clearStone;
			
			//aTree = new Tree (curCell, wiList, biList);
		}
		
		curTree = new Tree (curCell, wiList, biList);
		
		long startTime = System.nanoTime();
		
		
		String message = ""; 
		if (humTurn) {
			if ((humPlayer.color).equals("w")) {
				bestChosenCell = curTree.createMinMaxTree(aDepth, curCell, true, curBoard.retAllCells());
			}
			else {
				bestChosenCell = curTree.createMinMaxTree(aDepth, curCell, false, curBoard.retAllCells());
			}
			message = "Human best tile to play: Tile of color " + bestChosenCell.color + "on : ( " + bestChosenCell.xPos + "," + bestChosenCell.yPos + ")" ;
		}
		else {
			if ((compPlayer.color).equals("w") ) {
				bestChosenCell = curTree.createMinMaxTree(aDepth, curCell, true, curBoard.retAllCells());
			}
			else {
				bestChosenCell = curTree.createMinMaxTree(aDepth, curCell, false, curBoard.retAllCells());
			}
			message = "Computer best tile to play: Tile of color " + bestChosenCell.color + "on : ( " + bestChosenCell.xPos + "," + bestChosenCell.yPos + ")" ;
		}
		
		long endTime = System.nanoTime();

		long duration = (endTime - startTime)/1000000;
		 
		//displayMessage(); 
		newDisplay(duration);
		displayCurHeur();
		
	}

	/** 
	 * Function Name: newDisplay
	   Purpose: updated display to incorporate time take  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	
	public void newDisplay (long a) {
		TextView textView = (TextView)findViewById(R.id.t_turn);
		String message = "Turn: "; 
		//String note = ""; 
		String tColor = ""; 
		if (humTurn) {
			message += "HUMAN"; 
		}
		else {
			message += "COMPUTER";
		}		
		textView.setText(message); 
		textView = (TextView)findViewById(R.id.t_hint);
		message = "";
		message += "best tile to play: Tile of color " +  bestChosenCell.retTempColor() + " on : ( " + bestChosenCell.xPos + " , " + bestChosenCell.yPos + " ) \r\n" ; 
		message += " Time Taken : " + a ; 
		textView.setText(message);
		
	}

	/** 
	 * Function Name: respond
	   Purpose: update the button and board by the chosen button by user  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void respond(View view) {
		
		int cellNum = 0; 
		if ( getResources().getResourceEntryName(view.getId()).charAt(0) == 'a'  ) {
			
			
			
			cellNum =  Character.getNumericValue(getResources().getResourceEntryName(view.getId()).charAt(1)) * 10;
			cellNum += Character.getNumericValue(getResources().getResourceEntryName(view.getId()).charAt(2));
			cellNum -= 1;
			
			curCell = curBoard.retCell(cellNum); 
			curCell.color = chosenColor;
			
			bestChosenCell = curCell; 
			
			//set on commit , make curCell to null there
			curBoard.setCell(curCell); 
			
			if (chosenColor.equals("b")) {
				findViewById(view.getId()).getBackground().setColorFilter(0xFF000000, PorterDuff.Mode.MULTIPLY);
			}
			else if (chosenColor.equals("w")) {
				findViewById(view.getId()).getBackground().setColorFilter(0xFF003399, PorterDuff.Mode.MULTIPLY);
			}
			else if (chosenColor.equals("c")){
				findViewById(view.getId()).getBackground().setColorFilter(0xCCCC3399, PorterDuff.Mode.MULTIPLY);
			}
			
		}	
	}
	
	/** 
	 * Function Name: discardSuperfluousButtons
	   Purpose: Remove the extra set of buttons from the 11*11 view  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void discardSuperfluousButtons(){ 
		ViewGroup viewGroup = (ViewGroup)findViewById(R.id.i01).getParent(); 
		for (int i = 0 ; i < viewGroup.getChildCount(); i++) {
			if ( getResources().getResourceEntryName(viewGroup.getChildAt(i).getId()).charAt(0) == 'i'  ) {
				findViewById(viewGroup.getChildAt(i).getId()).getBackground().setColorFilter(0x00FFFFFF, PorterDuff.Mode.MULTIPLY);
			} 
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_board, menu);
		return true;
	}

}
