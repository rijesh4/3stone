package edu.ramapo.rkumar.stone;

import java.util.ArrayList;

public class Tree extends Board {
	
	private Cell headNode;  
	private int [] wL;
	private int [] bL; 
	//private Cell[] listCells; 
	
	Tree(Cell aCell, int[] wL, int[] bL){ // w,b,c
		headNode = aCell; 
		this.wL = wL; 
		this.bL = bL; 
	}

/** 
	 * Function Name: computeSingleHeuristics
	   Purpose: compute the heuristic value of a particular cell by chosen color
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) make the cell of chosen color
	2) compute score, update the WScore amd BScore, and change the color back to red  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void computeSingleHeuristics(Cell[] cellList, Cell curCell, String colorPlayed) {
		if (colorPlayed.equals("r")) {
			return; 
		}
		this.boardCells = cellList; 
		curCell.color = colorPlayed;
		//
		setCell(curCell);
		//boardCells[cellNumToPosConverter(curCell)] = curCell ;
		calcScore();
		curCell.heurVal[0] = WScore ;
		curCell.heurVal[1] = BScore;
		
		curCell.color = "r"; 
		setCell(curCell);
		
	}
	
	
	/*public void computeHeuristics(Cell[] cellList, Cell curCell, String colorPlayed){
		if (colorPlayed.equals("r")) {
			return; 
		}
		//listCells = cellList;
		ArrayList<Cell> lCells = retValidMoveCells(curCell, cellList); 
		//Cell tempCell[] = new Cell[80];
		this.boardCells = cellList; 
		Cell aCell = new Cell(-1,-1); 
		
		//cellList[0].xPos = 100; 
		//boardCells[0].xPos = 100; 
		
		for (int i = 0 ; i < 2 ; i++) { //lCells.size()
			aCell = lCells.get(i); 
			//tempCell[cellNumToPosConverter(aCell)] ; 
			aCell.color = colorPlayed;
			//
			setCell(aCell);
			//boardCells[cellNumToPosConverter(aCell)] = aCell ;
			calcScore();
			aCell.heurVal[0] = WScore ;
			aCell.heurVal[1] = BScore;
			aCell.color = "r"; 
			setCell(aCell);
			//
			//boardCells[cellNumToPosConverter(aCell)] = lCells.get(i) ;
		}
	}*/

	/** 
	 * Function Name: getBestChosenColor
	   Purpose: find the best Color to play for User of computer  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Chose the color one by one to get the max heuristic value 
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	
	public void getBestChosenColor (Cell[] cellList, Cell curCell, boolean isMaximizing) {
		
		String curChosenColor = ""; 
		if (isMaximizing) {
			curChosenColor = "w";
		}
		else if (!isMaximizing) {
			curChosenColor = "b";
		}
		
		int tempScore = 0;
		//
		if (isMaximizing ) {
			//computeSingleHeuristics(cellList, curCell, curChosenColor);
			//tempScore = WScore - BScore;
			if ( wL[0] > 0) {
				computeSingleHeuristics(cellList, curCell, "w");
				//tempScore = WScore - BScore;
				if (tempScore <  WScore - BScore) {
					curChosenColor = "w"; 
					tempScore = WScore - BScore;
				}
				
			}
			
			if ( wL[1] > 0) {
				computeSingleHeuristics(cellList, curCell, "b");
				//tempScore = WScore - BScore;
				if (tempScore <  WScore - BScore) {
					curChosenColor = "b"; 
					tempScore = WScore - BScore;
				}
				
			}
			if (wL[2] > 0) {
				computeSingleHeuristics(cellList, curCell, "c");
				if (tempScore <  WScore - BScore) {
					curChosenColor = "c"; 
					tempScore = WScore - BScore;
				} 
			}
			curCell.setTempColor(curChosenColor);
		}
		else if (!isMaximizing ) {
			if ( bL[1] > 0) {
				computeSingleHeuristics(cellList, curCell, "b");
				//tempScore = WScore - BScore;
				if (tempScore <  WScore - BScore) {
					curChosenColor = "w"; 
					tempScore = WScore - BScore;
				}
				
			}
			
			if ( bL[0] > 0) {
				computeSingleHeuristics(cellList, curCell, "w");
				//tempScore = WScore - BScore;
				if (tempScore <  WScore - BScore) {
					curChosenColor = "b"; 
					tempScore = WScore - BScore;
				}
				
			}
			if (bL[2] > 0) {
				computeSingleHeuristics(cellList, curCell, "c");
				if (tempScore <  WScore - BScore) {
					curChosenColor = "c"; 
					tempScore = WScore - BScore;
				} 
			}
			curCell.setTempColor(curChosenColor);
		}
		  
		
		
	}

/** 
	 * Function Name: createMinMaxTree
	   Purpose: Compute the best possible move using the minMax algorithm  
       Parameters:  depth, curCell, boolean isMaximizing, cellList
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) The algorithm uses recursion with base case for depth = 0, where it returns the curCell
	2) If base not 0, then computer the maximizing and minimizing alternatively.
	3) in each case it gets the list of all possible moves, and calls itself with reduced depth and maximizing/minimizing  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	
	public Cell createMinMaxTree(int depth, Cell curCell, boolean isMaximizing, Cell[] cellList) {
		// needs to return the most efficient node 
		//int bestHeurVal = -100; 
		
		
		if (depth == 0) {
			return curCell; 
		}
		
		//make sure that (-1,-1) is not returned to the calling function
		// If (-1,-1) , send any valid move .. 
		if (isMaximizing) {
			//int bestHeurVal = -100;
			Cell bestValNode = new Cell (-1,-1); 
			Cell curChosenTile = new Cell(-1,-1); 
		
			
			
			//ArrayList<Cell> lCell = new ArrayList(); 
			//lCell = retValidMoveCells(curCell); 
			curCell.leafChild = retValidMoveCells(curCell, cellList);
			
			for (int i = 0 ; i < curCell.leafChild.size() ; i++) { 
				curCell.leafChild.get(i).parent = curCell;
				//
				getBestChosenColor(cellList, curCell.leafChild.get(i), true);
				curChosenTile = createMinMaxTree(depth - 1, curCell.leafChild.get(i) , false , cellList); 
				//bestValNode = curCell.leafChild.get(i);
				calcScore();
				if ( ((bestValNode.heurVal[0] - bestValNode.heurVal[1] <=  curChosenTile.heurVal[0] - curChosenTile.heurVal[1]) && curChosenTile.xPos != -1) || bestValNode.xPos ==-1 ){
					bestValNode = curChosenTile; 
				}		
				curCell.leafChild.get(i).color = "r";
			}
			if (bestValNode.color.equals("w")) {
				wL[0]--; 
			}
			else if (bestValNode.color.equals("b")) {
				wL[1]--;
			}
			else if (bestValNode.color.equals("c")) {
				wL[2]--;
			}
			
			return bestValNode; 
		}
		else {
			
			Cell bestValNode = new Cell (-1,-1); 
			Cell curChosenTile = new Cell(-1,-1); 
			//ArrayList<Cell> lCell = new ArrayList(); 
			//lCell = retValidMoveCells(curCell); 
			curCell.leafChild = retValidMoveCells(curCell, cellList);
			
			for (int i = 0 ; i < curCell.leafChild.size() ; i++) { 
				curCell.leafChild.get(i).parent = curCell;
				//
				getBestChosenColor(cellList, curCell.leafChild.get(i), false);
				curChosenTile = createMinMaxTree(depth - 1, curCell.leafChild.get(i) , true , cellList); 
				//bestValNode = curCell.leafChild.get(i);
				calcScore();
				if ( ((bestValNode.heurVal[1] - bestValNode.heurVal[0] <=  curChosenTile.heurVal[1] - curChosenTile.heurVal[0] ) && curChosenTile.xPos != -1)|| bestValNode.xPos ==-1 ){
					bestValNode = curChosenTile; 
				}		
				curCell.leafChild.get(i).color = "r";
			}
			
			if (bestValNode.color.equals("w")) {
				bL[0]--; 
			}
			else if (bestValNode.color.equals("b")) {
				bL[1]--;
			}
			else if (bestValNode.color.equals("c")) {
				bL[2]--;
			}
			return bestValNode; 
			
		}
		
	}
	
	public void createMinMaxWithPruning (int depth, Cell curCell) {
		
		
	}
	
	/** 
	 * Function Name: retValidMoveCells
	   Purpose: return the list of valid moves a player can make given a cell  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public ArrayList<Cell> retValidMoveCells(Cell aCell, Cell[] cellList){
		
		//int leafCount = 0 ;
		Cell curHead = new Cell(-1,-1);
		Cell temp; // = headNode;
		//int depthCount = 0; 
		
		ArrayList<Cell> tempHeads = new ArrayList();
		ArrayList<Cell> recurHeads = new ArrayList(); 
		tempHeads.add(aCell); 
		curHead = aCell; 
		
		//while(depthCount < depth) {
			//depthCount++; 
			//tempHeads = recurHeads; 
			while (!tempHeads.isEmpty()) {
				curHead = tempHeads.get(0); 
				tempHeads.remove(0);
				temp = curHead; 
				//
				/*if (temp.xPos == 6 || temp.yPos == 6) {
					
				}*/
				//else { 
					//while cndition needs temp on the child here .. crashes
					while (temp.retLChild().xPos != -1 || (temp.xPos == 7 && temp.yPos == 6) ) {
						if (temp.retLChild().xPos == -1 ) {
							temp = temp.retLChild(); 
							continue; 
						}
						if (!temp.retLChild().color.equals("r") && !(temp.xPos == 7 && temp.yPos == 6)) {
							temp = temp.retLChild(); 
							continue; 
						}
						curHead.leafChild.add(temp.retLChild()); 
						temp.retLChild().parent = curHead;
						recurHeads.add(temp.retLChild()); 
						temp = temp.retLChild(); 
					}
					temp = curHead; 
					while (temp.retTChild().xPos != -1 || (temp.xPos == 6 && temp.yPos == 7)) {
						if (temp.retTChild().xPos == -1) {
							temp = temp.retTChild();
							continue; 
						}
						if (!temp.retTChild().color.equals("r") && !(temp.xPos == 6 && temp.yPos == 7)) {
							temp = temp.retTChild(); 
							continue; 
						}
						curHead.leafChild.add(temp.retTChild()); 
						temp.retTChild().parent = curHead; 
						recurHeads.add(temp.retTChild());
						temp = temp.retTChild(); 
					}
					temp = curHead; 
					while (temp.retRChild().xPos != -1  || (temp.xPos == 5 && temp.yPos == 6)) {
						if (temp.retRChild().xPos == -1) {
							temp = temp.retRChild();
							continue; 
						}
						if (!temp.retRChild().color.equals("r") && !(temp.xPos == 5 && temp.yPos == 6)) {
							temp = temp.retRChild(); 
							continue; 
						}
						curHead.leafChild.add(temp.retRChild()); 
						temp.retRChild().parent = curHead; 
						recurHeads.add(temp.retRChild());
						temp = temp.retRChild(); 
					}
					temp = curHead; 
					while (temp.retBChild().xPos != -1 || (temp.xPos == 6 && temp.yPos == 5)) {
						if (temp.retBChild().xPos == -1) {
							/*if(temp.xPos == 6 && temp.yPos == 5) {
								//curHead.leafChild.add(boardCells); 
							}*/
							temp = temp.retBChild();
							continue; 
						}
						if (!temp.retBChild().color.equals("r") && !(temp.xPos == 6 && temp.yPos == 5)) {
							temp = temp.retBChild(); 
							continue; 
						}
						curHead.leafChild.add(temp.retBChild()); 
						temp.retBChild().parent = curHead; 
						recurHeads.add(temp.retBChild());
						temp = temp.retBChild(); 
					}
					if (recurHeads.size() == 0) {
						for (int i = 0 ; i <cellList.length ; i++ ) {
							if (!cellList[i].color.equals("r")) {
								recurHeads.add(cellList[i]); 
							}
						}
					}
					 
					/*for (int k = 0 ; k < recurHeads.size(); k++) {
						System.out.println(recurHeads.get(k).xPos + " " + recurHeads.get(k).yPos); 
					}*/
				//}
			}//inner while
			return recurHeads;
		//}//outer while
	}

	
	
}
