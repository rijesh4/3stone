package edu.ramapo.rkumar.stone;

public class Board {
	
	protected Cell boardCells[] = new Cell[81]; 
	private int numOfX[] = {4,6,8,10,12,12,12,10,8,6,4}; 
	private String colorAss = new String("r"); 
	protected int BScore = 0; 
	protected int WScore = 0; 
	//Need humHandCells and CompHandCells -> 15 each 
	//Need BlankCells
	
	Board () {
		for (int i = 0 ; i <= 80; i++) {
			boardCells[i] = new Cell(); 
		}
		initCells(); 
		initChildren1();
	}
	
	/** 
	 * Function Name: initChildren1
	   Purpose: Initialize the children of each cell  
       Parameters:  None
	   Return Value: void
	   Local Variables: 
	   		
Algorithm: 
	1) Check for the bounds and find the valid cell
	2) Add the children l , r , t ,b to the cell  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void initChildren1() {
		//Cell aCell = new Cell(-1, -1); 
		boardCells[80] = new Cell (-1,-1); 
		boardCells[80].setLChild(boardCells[cellNumToPosConverter(new Cell(5,6))]); 
		boardCells[80].setTChild(boardCells[cellNumToPosConverter(new Cell(6,5))]);
		boardCells[80].setRChild(boardCells[cellNumToPosConverter(new Cell(7,6))]);
		boardCells[80].setBChild(boardCells[cellNumToPosConverter(new Cell(6,7))]);
		
				
		for (int i = 0 ; i < 80 ; i++) {
			//aCell = boardCells[i]; 
			
			for (int j = 0 ; j < 4 ; j++) {
				Cell temp = new Cell (-1, -1);
				switch(j) {
				case 0: 
					if ((boardCells[i].xPos == 7 && boardCells[i].yPos==6) ) {
						boardCells[i].setLChild(boardCells[80]); 
						boardCells[i].retLChild().setLChild(boardCells[cellNumToPosConverter(new Cell(5,6))]); // remove these. redundant
						break;
					}
					temp = new Cell (boardCells[i].xPos - 1, boardCells[i].yPos);
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						boardCells[i].setLChild(new Cell(-1,-1)); 
					}
					else {
						boardCells[i].setLChild(boardCells[cellNumToPosConverter(temp)]);
					}
					break; 
				case 1:
					if ((boardCells[i].xPos == 6 && boardCells[i].yPos==7) ) {
						boardCells[i].setTChild(boardCells[80]);
						boardCells[i].retTChild().setTChild(boardCells[cellNumToPosConverter(new Cell(6,5))]);
						break;
					}
					else if (boardCells[i].yPos == 7 || boardCells[i].yPos == 6) {
						temp = new Cell (boardCells[i].xPos , boardCells[i].yPos - 1);
					}
					else if (boardCells[i].yPos >= 8) {
						temp = new Cell (boardCells[i].xPos + 1, boardCells[i].yPos - 1);
					}
					else{
						temp = new Cell (boardCells[i].xPos -1 , boardCells[i].yPos - 1);
					}
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						boardCells[i].setTChild(new Cell(-1,-1)); 
					}
					else {
						boardCells[i].setTChild(boardCells[cellNumToPosConverter(temp)]);
					}
					break;
				case 2: 
					if ((boardCells[i].xPos == 5 && boardCells[i].yPos==6) ) {
						boardCells[i].setRChild(boardCells[80]);
						boardCells[i].retRChild().setRChild(boardCells[cellNumToPosConverter(new Cell(7,6))]);
						break;
					}
					temp = new Cell (boardCells[i].xPos + 1, boardCells[i].yPos);
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						boardCells[i].setRChild(new Cell(-1,-1)); 
					}
					else {
						boardCells[i].setRChild(boardCells[cellNumToPosConverter(temp)]);
					}
					break;
				case 3: 
					if ((boardCells[i].xPos == 6 && boardCells[i].yPos==5) ) {
						boardCells[i].setBChild(boardCells[80]); 
						boardCells[i].retBChild().setBChild(boardCells[cellNumToPosConverter(new Cell(6,7))]);
						break;
					}
					else if (boardCells[i].yPos == 5 || boardCells[i].yPos == 6) {
						temp = new Cell (boardCells[i].xPos , boardCells[i].yPos + 1);
					}
					else if (boardCells[i].yPos >= 7) {
						temp = new Cell (boardCells[i].xPos - 1, boardCells[i].yPos + 1);
					}
					else{
						temp = new Cell (boardCells[i].xPos +1 , boardCells[i].yPos + 1);
					}
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						boardCells[i].setBChild(new Cell(-1,-1)); 
					}
					else {
						boardCells[i].setBChild(boardCells[cellNumToPosConverter(temp)]); 
					}
				break; 	
				}
			}
			//boardCells[i] = aCell; 
			//System.out.println(boardCells[i].xPos+" "+boardCells[i].yPos + ": BC : " +  boardCells[i].retBChild().xPos+" "+boardCells[i].retBChild().yPos);
		}
		//int x = 3; 
	}
	
/** 
	 * Function Name: CalcScore
	   Purpose: Calculate the score of the cur Board  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Find The arrangement vertHor and diagonals for each cell
	2) If a valid arrangement increase the score  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void calcScore() {
		
		Cell curCell = new Cell(-1,-1); 
		int B1score = 0;
		int W1score = 0; 
		for (int i = 0 ; i < 79 ; i++) {
			curCell = boardCells[i]; 
			if (curCell.color.equals("r") ) {
				continue; 
			}
			else {
				
				for (int j = 0 ; j < 4 ; j++) {
					if (isVertHor(j , curCell) ) {
						if (curCell.color.equals("b") ) {
							B1score++;
						} 
						else if (curCell.color.equals("w") ) {
							W1score++;
						}
						else if (curCell.color.equals("c") ) {
							if ( colorAss.equals("bb") ||  colorAss.equals("bc") || colorAss.equals("cb")) {
								B1score++;
							}
							else if (colorAss.equals("ww") ||  colorAss.equals("wc") || colorAss.equals("cw")) {
								W1score++;
							}
						}
					}
					if (isDiagonal(j, curCell)) {
						if (curCell.color.equals("b") ) {
							B1score++;
						} 
						else if (curCell.color.equals("w")) {
							W1score++;
						}
						else if (curCell.color.equals("c") ) {
							if ( colorAss.equals("bb") ||  colorAss.equals("bc") || colorAss.equals("cb")) {
								B1score++;
							}
							else if (colorAss.equals("ww") ||  colorAss.equals("wc") || colorAss.equals("cw")) {
								W1score++;
							}
						} 
					}
				}
			}			
		}
		BScore = B1score/2; 
		WScore = W1score/2; 
	}
	
	/** 
	 * Function Name: isVertHor
	   Purpose: Calculate the score of the arrangement in vertical and horizontal organization  
       Parameters:  val , aCell 
	   Return Value: bool
	   Local Variables: None
	   		
Algorithm: 
	1) Check the left and right bounds 
	2) If valid return true   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	//val: 0-l, 1 t, 2r, 3b
	public boolean isVertHor(int val, Cell aCell) {
		colorAss = "";
		Cell temp = new Cell(-1, -1); 
		switch(val){
		case 0:
			if ((aCell.xPos == 7 && aCell.yPos==6) ||(aCell.xPos == 8 && aCell.yPos==6) ) {
				return false; 
			}
			temp = new Cell (aCell.xPos - 1, aCell.yPos);
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")|| aCell.color.equals("c") ) {
					temp = new Cell(temp.xPos -1 , temp.yPos); 
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c") || aCell.color.equals("c")  ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			
			return false;
			
		case 1:
			if ((aCell.xPos == 6 && aCell.yPos==7) ||(aCell.xPos == 5 && aCell.yPos==8) ) {
				return false; 
			}
			if (aCell.yPos == 7 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos , aCell.yPos - 1);
			}
			else if (aCell.yPos >= 8) {
				temp = new Cell (aCell.xPos + 1, aCell.yPos - 1);
			}
			else{
				temp = new Cell (aCell.xPos -1 , aCell.yPos - 1);
			}
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals( aCell.color) || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c")   ) {
					if (temp.yPos == 7 || temp.yPos == 6) {
						temp = new Cell(temp.xPos , temp.yPos - 1);
					}
					else if (temp.yPos >= 8) {
						temp = new Cell (temp.xPos + 1, temp.yPos - 1);
					}
					else{
						temp = new Cell(temp.xPos -1 , temp.yPos - 1);
					} 
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c") ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			
			return false;
			
		case 2:
			if ((aCell.xPos == 4 && aCell.yPos==6) ||(aCell.xPos == 5 && aCell.yPos==6) ) {
				return false; 
			}
			temp = new Cell (aCell.xPos + 1, aCell.yPos);
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c") || aCell.color.equals("c")   ) {
					temp = new Cell(temp.xPos +1 , temp.yPos);
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c") || aCell.color.equals("c") ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			
			return false;
			
		case 3:
			if ((aCell.xPos == 6 && aCell.yPos==5) ||(aCell.xPos == 5 && aCell.yPos==4) ) {
				return false; 
			}
			if (aCell.yPos == 5 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos , aCell.yPos + 1);
			}
			else if (aCell.yPos >= 7) {
				temp = new Cell (aCell.xPos - 1, aCell.yPos + 1);
			}
			else{
				temp = new Cell (aCell.xPos +1 , aCell.yPos + 1);
			}
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c")  ) {
					if (temp.yPos == 5 || temp.yPos == 6) {
						temp = new Cell(temp.xPos , temp.yPos + 1);
					}
					else if (temp.yPos >= 7) {
						temp = new Cell (temp.xPos - 1, temp.yPos + 1);
					}
					else{
						temp = new Cell(temp.xPos +1 , temp.yPos + 1);
					}  
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c") ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			return false;	
		}
		return false; 
	}
	
		/** 
	 * Function Name: isDiagonal
	   Purpose: Calculate the score of the arrangement in Diagonal organization  
       Parameters:  val , aCell 
	   Return Value: bool
	   Local Variables: None
	   		
Algorithm: 
	1) Check the diagonal bounds 
	2) If valid return true   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	//val: 0-tl, 1 tr, 2bl, 3br
	public boolean isDiagonal(int val, Cell aCell) {
		colorAss = ""; 
		Cell temp = new Cell(-1, -1); 
		
		switch(val) {
		case 0:
			if ((aCell.xPos == 7 && aCell.yPos==7) ||(aCell.xPos == 7 && aCell.yPos==8) ) {
				return false; 
			}
			if (aCell.yPos == 7 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos - 1, aCell.yPos - 1);
			}
			else if (aCell.yPos >= 8) {
				temp = new Cell (aCell.xPos, aCell.yPos - 1);
			}
			else{
				temp = new Cell (aCell.xPos - 2, aCell.yPos - 1);
			} 
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c")  ) {
					if (temp.yPos == 7 || temp.yPos == 6) {
						temp = new Cell(temp.xPos -1 , temp.yPos -1);
					}
					else if (temp.yPos >= 8) {
						temp = new Cell (temp.xPos, temp.yPos - 1);
					}
					else{
						temp = new Cell(temp.xPos -2 , temp.yPos -1);
					}  
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c") || aCell.color.equals("c") ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			} 
			
			return false;
		case 1:
			if ((aCell.xPos == 5 && aCell.yPos==7) ||(aCell.xPos == 3 && aCell.yPos==8) ) {
				return false; 
			}
			if (aCell.yPos == 7 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos + 1, aCell.yPos - 1);
			}
			else if (aCell.yPos >= 8) {
				temp = new Cell (aCell.xPos + 2, aCell.yPos - 1);
			}
			else{
				temp = new Cell (aCell.xPos, aCell.yPos - 1);
			} 
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]    ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals( aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c") ) {
					if (temp.yPos == 7 || temp.yPos == 6) {
						temp = new Cell(temp.xPos+1 , temp.yPos -1);
					}
					else if (temp.yPos >= 8) {
						temp = new Cell (temp.xPos + 2, temp.yPos - 1);
					}
					else{
						temp = new Cell(temp.xPos , temp.yPos -1);
					}
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]   ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color) || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c")) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			return false;
			
		case 2:
			if ((aCell.xPos == 7 && aCell.yPos==5) ||(aCell.xPos == 7 && aCell.yPos==4) ) {
				return false; 
			}
			if (aCell.yPos == 5 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos -1 , aCell.yPos +1);
			}
			else if (aCell.yPos >= 7) {
				temp = new Cell (aCell.xPos - 2, aCell.yPos + 1);
			}
			
			else{
				temp = new Cell (aCell.xPos, aCell.yPos +1);
			}  
			//String a = boardCells[cellNumToPosConverter(temp)].color; 
			//colorAss += boardCells[cellNumToPosConverter(temp)].color; 
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c") ) {
					if (temp.yPos == 5 || temp.yPos == 6) {
						temp = new Cell(temp.xPos - 1 , temp.yPos +1);
					}
					else if (temp.yPos >= 7) {
						temp = new Cell (temp.xPos - 2, temp.yPos + 1);
					}
					else{
						temp = new Cell(temp.xPos , temp.yPos +1);
					}  
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals("c") ) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			} 
			return false;
			
		case 3:
			if ((aCell.xPos == 5 && aCell.yPos==5) ||(aCell.xPos == 3 && aCell.yPos==4) ) {
				return false; 
			}
			if (aCell.yPos == 5 || aCell.yPos == 6) {
				temp = new Cell (aCell.xPos + 1, aCell.yPos + 1);
			}
			else if (aCell.yPos >= 7) {
				temp = new Cell (aCell.xPos, aCell.yPos + 1);
			}
			else{
				temp = new Cell (aCell.xPos + 2, aCell.yPos + 1);
			}  
			if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
				return false; 
			}
			else {
				colorAss += boardCells[cellNumToPosConverter(temp)].color;
				if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c")  || aCell.color.equals( "c") ) {
					if (temp.yPos == 5 || temp.yPos == 6) {
						temp = new Cell(temp.xPos + 1 , temp.yPos +1);
					}
					else if (temp.yPos >= 7) {
						temp = new Cell (temp.xPos, temp.yPos + 1);
					}
					else{
						temp = new Cell(temp.xPos + 2 , temp.yPos +1);
					} 
					if (temp.yPos>11 || temp.yPos <= 0 || temp.xPos <=0 || temp.xPos >= numOfX[temp.yPos - 1]  ) {
						return false; 
					}
					else if (boardCells[cellNumToPosConverter(temp)].color.equals(aCell.color)  || boardCells[cellNumToPosConverter(temp)].color.equals("c") || aCell.color.equals("c")) {
						colorAss += boardCells[cellNumToPosConverter(temp)].color;
						return true; 
					}
				}
				else {
					return false; 
				}
			}
			return false;
		}
		
		return false; 
	}
	
	/** 
	 * Function Name: resetAllTempData
	   Purpose: Resets the temp data populated from MinMax tree while computing the best move 
       Parameters:  
	   Return Value: 
	   Local Variables: None
	   		
Algorithm:    
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */	
	public void resetAllTempData() { //removes the heurVal , leafChild, and Parent . (temp created for tree)
		for (int i = 0 ; i <= 79 ; i++) {
			boardCells[i].heurVal[0] = 0; 
			boardCells[i].heurVal[1] = 0;
			boardCells[i].leafChild.clear(); 
			boardCells[i].parent = new Cell(-1,-1); 
		}
	}
	
	
		/** 
	 * Function Name: cellNumToPositionConverter
	   Purpose: Calculates the position in the board givent a Cell  
       Parameters:   aCell 
	   Return Value: int
	   Local Variables: None
	   		
Algorithm:    
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public int cellNumToPosConverter(Cell aCell){
		switch(aCell.yPos){
		
		case 1:
			return aCell.xPos -1;  
		case 2:
			return 3 + aCell.xPos -1; 
		case 3:
			return 8 + aCell.xPos -1; 
		case 4:
			return 15 + aCell.xPos -1;
		case 5:
			return 24 + aCell.xPos -1;
		case 6:
			if (aCell.xPos <=5) {
				return 35 + aCell.xPos -1;
			}
			else {
				return 35 + aCell.xPos -2; 
			}
		case 7:
			return 45 + aCell.xPos -1;
		case 8:
			return 56 + aCell.xPos -1;
		case 9:
			return 65 + aCell.xPos -1;
		case 10:
			return 72 + aCell.xPos -1;
		case 11:
			return 77 + aCell.xPos -1;
		
		default:
			return 0; 
		}	
	}

		/** 
	 * Function Name: initCells
	   Purpose: Initialize the curBoard with cells with correct x, y positions  
       Parameters:   
	   Return Value: 
	   Local Variables: 
	   		
Algorithm:   
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void initCells() { 
		int numColumns = 3; 
		int cellCount = 0; 
		int revCellCount = 79; 
		//int sixthColumn = 0; 
		
		for (int i = 1 ; i <= 6 ; i++) {
			for (int j = 1 ; j <= numColumns ; j++  ) {
				boardCells[cellCount++] = new Cell(j , i); 
				boardCells[revCellCount--] = new Cell (numColumns+1-j , 11+1 - i ); 
				if (i == 6 && j == 5) {
					j++; 
				}
				
			}
			if (numColumns == 3 ||numColumns == 5 || numColumns == 7  || numColumns == 9) {
				numColumns += 2; 
			}		
		}
	}

	//Getters and setters
	public Cell [] retAllCells() {
		return boardCells; 
	}
	
	public Cell retCell (int pos) {
		return boardCells[pos]; 
	}
	
	public void setCell ( Cell aCell) {
		boardCells[cellNumToPosConverter(aCell)] = aCell ; 
	}
	
	
	
}
