package edu.ramapo.rkumar.stone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

public class MainActivity extends Activity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	
	
	public void startNewGame(View view){
		Intent intent = new Intent(this, TurnSelect.class);
		//intent.putExtra("humTurn", humTurn); 
		startActivity(intent);
	}
	
	/** 
	 * Function Name: loadGame
	   Purpose: load the game from file  
       Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
Algorithm: 
	1) Used matcher to find the appropriate score, tiles in hands and the board configuration  
 Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
 */
	public void loadGame(View view){
		
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,"myFile123.txt");
		String line = ""; 
		ParcelData pd = new ParcelData(); 
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine(); 
			
			int tempInt[] = {0,0,0,0}; 
			int tempCount = 0; 
			
			Pattern pattern = Pattern.compile("Human Player");
	        Matcher matcher = pattern.matcher(line);
	        if (matcher.find()) {
	        	pattern = Pattern.compile("\\b\\d\\d?\\b");
		        matcher = pattern.matcher(line);
		        while (matcher.find()){
		        	tempInt[tempCount++] = Integer.parseInt(matcher.group());  
		        }
		        pd.score[0] = tempInt[0];
	        	pd.setHumStone(tempInt[1] , 0 );
	        	pd.setHumStone (tempInt[2]  , 1 );
	        	pd.setHumStone ( tempInt[3] , 2 );
	        }
	        tempCount = 0; 
	        
	        pattern = Pattern.compile("Black");
	        matcher = pattern.matcher(line);
	        if (matcher.find()) {
	        	pd.setHumColor("b");
				pd.setCompColor("w");
	        }
	        else {
	        	pd.setHumColor("w");
				pd.setCompColor("b");
	        }
			
		
		//Line 2	
			line = br.readLine(); 
			
			
			pattern = Pattern.compile("Computer Player");
	        matcher = pattern.matcher(line);
	        if (matcher.find()) {
	        	pattern = Pattern.compile("\\b\\d\\d?\\b");
		        matcher = pattern.matcher(line);
		        while (matcher.find()){
		        	tempInt[tempCount++] = Integer.parseInt(matcher.group()); 
		        }
		        pd.score[1] = tempInt[0];
	        	pd.setCompStone ( tempInt[1] , 0 );
	        	pd.setCompStone ( tempInt[2] , 1 );
	        	pd.setCompStone ( tempInt[3] , 2 );
	        }
	        tempCount = 0; 
		//Line 3
			line = br.readLine(); 
			
			pattern = Pattern.compile("Human");
	        matcher = pattern.matcher(line);
	        if (matcher.find()) {
	        	pd.setHumTurn(true);
	        	pattern = Pattern.compile("\\b\\d\\d?\\b");
		        matcher = pattern.matcher(line);
		        while (matcher.find()){
		        	tempInt[tempCount++] = Integer.parseInt(matcher.group()); 
		        }
		        pd.curCell.xPos =  tempInt[1];
				pd.curCell.yPos =  tempInt[0];
	        }
	        else {
	        	pd.setHumTurn(false);
	        	pattern = Pattern.compile("\\b\\d\\d?\\b");
		        matcher = pattern.matcher(line);
		        while (matcher.find()){
		        	tempInt[tempCount++] = Integer.parseInt(matcher.group()); 
		        }
		        pd.curCell.xPos =  tempInt[0];
				pd.curCell.yPos =  tempInt[1];
	        }
			
		//Line 5
			line = br.readLine(); 
			//line = br.readLine(); 
		//Read Board; 
			int counter = 3; 
			int tempCounter = 0; 

			for (int i = 0 ; i < 11 ; i++) {
				
				line = br.readLine(); 
			
				for (int j = 0 ; j < counter ; j++) {
					if ( line.charAt((j)*2) ==  'O')  {
						pd.curBoard.add("r"); //[tempCounter+j] = "r"; 
					}
					else if (line.charAt((j)*2) ==  'B') {
						pd.curBoard.add("b"); 
					}
					else if (line.charAt((j)*2) ==  'W') {
						pd.curBoard.add("w");
					}
					else if (line.charAt((j)*2) ==  'C') {
						pd.curBoard.add("c");
					}
				}
				
				tempCounter += counter; 
				if (i <= 3 ) {
					counter += 2; 
				}
				else if (i == 5) {
					counter += 1; 
				}
				/*else if (i == 6) {
					counter += 1; 
				}*/
				else if (i >= 6 ) {
					counter -= 2; 
				}
				else if (i == 4) {
					counter -=1; 
				}
			}
			br.close(); 
		}				
		catch (IOException e) {
		    // proper error handling here
		}
		
		pd.setGameType("lg"); 
		Intent intent = new Intent(this, GameBoard.class);
		intent.putExtra("pdVal", pd);
		startActivity(intent);
		
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/

}
