package edu.ramapo.rkumar.stone;

public class Player {
	
	protected boolean isHumTurn; 
	protected int score;
	protected int blackStone; 
	protected int whiteStone; 
	protected int clearStone; 
	protected String color; 
	
	Player (){
		isHumTurn = false;
		score = 0;
		blackStone = 15; 
		whiteStone = 15; 
		clearStone = 15; 
		color = "b";
	}
	
	public void nextMove() {
		
	}
	
	//Getters and Setters
	public int retBlackStone () {
		return blackStone; 
	}
	public int retWhiteStone () {
		return whiteStone; 
	}
	public int retClearStone () {
		return clearStone; 
	}
	public int retStoneCount () {
		return blackStone + whiteStone + clearStone; 
	}
	
	
}