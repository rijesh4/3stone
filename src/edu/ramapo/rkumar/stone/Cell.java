package edu.ramapo.rkumar.stone;

import java.io.Serializable;
import java.util.ArrayList;

public class Cell implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6647223442021326702L;
	public String color = "r";
	private String tempColor = "r";  
	
	
	public int xPos = -1;
	public int yPos = -1; 
	public int heurVal[] = {0, 0  }; //wScore , bScore
	private Cell lChild;// = new Cell(-1,-1); 
	private Cell rChild;// = new Cell(-1,-1);
	private Cell tChild;// = new Cell(-1,-1);
	private Cell bChild;// = new Cell(-1,-1);
	public ArrayList<Cell> leafChild = new ArrayList() ;
	public Cell parent; 
	
	//Default Constructor
	Cell () {
		xPos = -1; 
		yPos = -1; 
		lChild = new Cell(-1,-1); 
		rChild = new Cell(-1,-1);
		tChild = new Cell(-1,-1);
		bChild = new Cell(-1,-1);
		//
		parent = new Cell(-1,-1); 
	}
	
	//Constructor
	Cell( int xPos, int yPos) {
		//this.color = color; 
		this.xPos = xPos; 
		this.yPos = yPos; 
		
	}

	//Getters and Setters
	public void setLChild(Cell aCell){
		lChild= aCell; 
	}
	
	public void setRChild(Cell aCell){
		rChild= aCell; 
	}
	
	public void setTChild(Cell aCell){
		tChild= aCell; 
	}
	
	public void setBChild(Cell aCell){
		bChild= aCell; 
	}
	
	public Cell retLChild () {
		return lChild; 
	}
	public Cell retRChild () {
		return rChild; 
	}
	public Cell retTChild () {
		return tChild; 
	}
	public Cell retBChild () {
		return bChild; 
	}
	public void setTempColor (String r) {
		tempColor = r; 
	}
	public String retTempColor () {
		return tempColor; 
	}
	

}
