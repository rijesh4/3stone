3 Stones

The Objective

3 Stones is a board game for two players. The players score points by playing their stones. When the game ends, the player with the most points wins.
The Players

Two players play this game - one player will be the human user of your program, and the other player will be your program/computer. The two players play the game till one of them wins.
The Setup

The board: The game is played on an octagonal board consisting of 80 pockets laid out in 11 rows and 11 columns, as shown below, where pockets are represented by O:
        O O O         
      O O O O O       
    O O O O O O O     
  O O O O O O O O O   
O O O O O O O O O O O
O O O O O   O O O O O
O O O O O O O O O O O
  O O O O O O O O O   
    O O O O O O O     
      O O O O O       
        O O O         
Note that the center row has an empty space in the center.
Stones:: The set up includes a pouch which contains 30 white stones, 30 black stones and 12 clear stones.

Score card: Finally, each player maintains a score card, initially set to zero.

A Round

Picking color: Initially, a coin is tossed and the human player is asked to call head or tail.
If the human player calls the coin toss correctly, the human player is asked to pick a color: white or black. The computer takes the other color.
If the human player calls the coin toss incorrectly, computer picks the color and human player takes the other color.
Each player gets 15 black stones, 15 white stones and 6 clear stones.
First player: The player who picked the black stone plays first. Thereafter, the two players alternate.
A play: The player places one of his stones in an empty pocket on the board.
Valid play:
If this is the first play of the round, the player can place it anywhere on the board.
Otherwise, the player must place the stone into an empty pocket either in the same row or in the same column as the opponent's last play (but not necessarily adjacent to the stone last played by the opponent).
If no pocket is available on the row and column of the opponent's last play, the stone can be placed into any empty pocket on the board.
Objective: The objective of each play is to earn points:
The player earns one point by placing 3 stones in adjacent pockets:
The stones may be in a row, column or diagonal
The stones must all be of the player's color or clear color
Clear stones count for either player. As a result, when a clear stone (C) is placed by either player, both players may get a point each, as in the following scenarios:
     B                             B
   W C W     W W C B B             B
     B                         W W C
But, a row of 3 clear stones will not count for either player
A stone may be part of more than one scoring arrangement, e.g., each of the following arrangements of white stones earns 2 points (lowercase w being the latest stone added):
                       W             W          W   W          W
W W w W            W W w           W w W          w            w
                       W             W          W   W          W
                                                               W
Note that a player does not have to have placed all the stones in an arrangement to be able to count points for the arrangement. In other words, some stones in the arrangement may be by the current player and others by the opponent.
After each play, the player will update his score card. The player will add points for only the new 3-stone arrangements that resulted from the play.
The game ends when the last stone is played.
Winning: When the game ends, the player with the most points wins. If both the players have the same number of points, the game is a draw.
Strategy

On every play, the player must determine:
Color to play: Whether to play a stone of his color, opponent's color, or clear color
Playing own color: The player may want to play his own color to:
Build towards a 3-stone arrangement in his color
If the player has more than one incomplete arrangement, complete the one that earns the most points.
Block the opponent from completing a 3-stone arrangement
Playing opponent's color: The player may want to play opponen't color so that the opponent cannot co-opt the stone to earn points, such as by placing the stone in a pocket surrounded by one's own stones.
Playing clear stones:
The player may want to hold on to clear stones and use them only after running out of his own stones
The player may want to avoid playing a clear stone when it may also benefit the opponent, as in the examples illustrated earlier.
Implementation

Serialization: The user should be able to suspend the game after any play, and resume at a later time from where the game was left off. In order to do this:
Provide the option to serialize after each play in the game.
When the serialization option is exercised, your program will save the current state of the game into a file and quit. We will use text format for the file as follows:
Human Player: Black 8 7 3 2
Computer Player: White 7 4 9 0
Next Player: Human 3 5
Board: 
O O O
O O B O O
O O O B W W O
O O O O C W O O O
O O O O O B W C O O O 
O O O O O O O O O O 
O O O O O O O O O O O
O O O O O O O O O   
O O O O O O O     
O O O O O       
O O O         
In the above format, human player currently has 8 points, 7 black, 3 white and 2 clear stones. Computer player currently has 7 points, 4 black, 9 white and 0 clear stones. It is the human player's turn to play next. The last pocket in which the opponent (in this case, computer) placed a stone before the game was suspended was on row 3 (top row is 1, bottom row is 11) and column 5 (leftmost column is 1, rightmost column is 11). Each row of the board is represented by a separate line of O (for open), W (for white), B (for black) or C (for clear).
When your program is started, it will provide the option to resume a game from a previously saved state. If yes, it will ask for the name of the text file from which to read the current state of the game, and resume playing from there onwards.
Use a function of the points earned by the players as your heuristic function.
Before each move (human or computer), the user should have the option to select the following:
The algorithm for computing the best next move: 1) minimax only; 2) minimax with alpha-beta pruning.
The number of plys to which to search for the best next move.
The computer must use minimax algorithm without or with alpha-beta pruning to the selected ply to determine its next move. Based on the algorithm(s), it must display all its possible next moves and their heuristic values on the board. It should also display the time taken to compute these moves. Finally, the computer should make its best move only after the user clicks a button to give it the go-ahead.
Similarly, when it is the user's turn, the user should be able to ask the computer to suggest a move. The computer must use minimax algorithm without or with alpha-beta pruning to the selected ply to determine this move. By way of clarifying the options available for the human player, it must display all possible moves available to the human player and their heuristic values on the board. It should also display the time it took to compute these moves for the user. After getting the computer's recommendations, the human player will make her/his move, which may or may not be the best recommended by the computer.
Before each move (human or computer), the user should be able to repeatedly change the algorithm and ply value and ask for the correct recommendation based on these - in the vein of what-if analysis.
Observe strict separation between model and view/controller.
Using any part of code available in textbooks or on the web is unacceptable.
Acknowledgments

This game was adapted from the description at EducationalLearningGames.com.